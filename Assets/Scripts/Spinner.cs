﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Spinner : MonoBehaviour {

    #region Fields and Properties

    /// <summary>
    /// The delta position.
    /// </summary>
    [SerializeField]
    Vector3 deltaPos = new Vector3(0, .25f, 0);

    /// <summary>
    /// The duration of the position.
    /// </summary>
    [SerializeField]
    float posCycleDuration = 1.25f;

    /// <summary>
    /// The delta rot.
    /// </summary>
    [SerializeField]
    Vector3 deltaRot = new Vector3(0, 360, 0);

    /// <summary>
    /// The duration of the rot.
    /// </summary>
    [SerializeField]
    float rotCycleDuration = 2.5f;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled
    /// </summary>
    private void Update()
    {
	
    }

    private void OnEnable()
    {
        if(transform.DOComplete() == 0)
        {

            transform.DORotate(transform.eulerAngles + deltaRot,
                               rotCycleDuration,
                               RotateMode.FastBeyond360)
                     .SetEase(Ease.Linear)
                     .SetLoops(-1);
            transform.DOMove(transform.position + deltaPos,
                             posCycleDuration)
                     .SetEase(Ease.Linear)
                     .SetLoops(-1, LoopType.Yoyo);
        }
    }

    private void OnDisable()
    {
        transform.DOKill();
    }

    #endregion

    #region Methods



    #endregion

}
